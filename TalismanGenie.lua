TalismanGenie = {}

function TalismanGenie.Initialise()
    CreateWindow("TalismanGenie", true)
    WindowSetShowing("TalismanGenie", false)
    isVisible=false
   -- Apply text to the label and button so they are not blank.
   LabelSetText("TalismanGenieTitleBarText", L"Talisman Genie")
   LabelSetText("TalismanGenieLevelLabel", L"Rank")
   LabelSetText("TalismanGenieFragmentLabel", L"Fragment")
   LabelSetText("TalismanGenieFragmentMinLabel", L"Min")
   LabelSetText("TalismanGenieFragmentMaxLabel", L"Max")
   LabelSetText("TalismanGenieContainerLabel", L"Use 200 Containers?")
   LabelSetText("TalismanGenieTypeLabel", L"Type")
   LabelSetText("TalismanGenieCommonLabel", L"+0")
   LabelSetText("TalismanGenieUncommonLabel", L"+0")
   LabelSetText("TalismanGenieRareLabel", L"+0")
   LabelSetText("TalismanGenieVeryRareLabel", L"+0")   
   LabelSetText("TalismanGenieGoldDustLabel", L"Gold Dust")
   LabelSetText("TalismanGenieGoldDustMinLabel", L"Min")
   LabelSetText("TalismanGenieGoldDustMaxLabel", L"Max")
   LabelSetText("TalismanGenieUseGoldDustLabel", L"Do not use Gold Dust?")
   LabelSetText("TalismanGenieEssenceLabel", L"Essence")
   LabelSetText("TalismanGenieEssenceMinLabel", L"Min")
   LabelSetText("TalismanGenieEssenceMaxLabel", L"Max")
   LabelSetText("TalismanGenieUseEssenceLabel", L"Do not use Essence?")
   LabelSetText("TalismanGenieCurioLabel", L"Curio")
   LabelSetText("TalismanGenieCurioMinLabel", L"Min")
   LabelSetText("TalismanGenieCurioMaxLabel", L"Max")
   LabelSetText("TalismanGenieUseCurioLabel", L"Do not use Curios?")
 --  LabelSetText("TalismanGenieColumnsLabel", L"Container  Fragment  Gold Dust  Essence  Curio")
    LabelSetText("TalismanGenieContainerColumnLabel", L"Container")
    LabelSetText("TalismanGenieFragmentColumnLabel", L"Fragment")
    LabelSetText("TalismanGenieGoldDustColumnLabel", L"Gold Dust")
    LabelSetText("TalismanGenieEssenceColumnLabel", L"Essence")
    LabelSetText("TalismanGenieCurioColumnLabel", L"Curio")
   
   
   ButtonSetCheckButtonFlag("TalismanGenieContainerCheckBox", true)
   ButtonSetCheckButtonFlag("TalismanGenieGoldDustCheckBox", true)
   ButtonSetCheckButtonFlag("TalismanGenieEssenceCheckBox", true)
   ButtonSetCheckButtonFlag("TalismanGenieCurioCheckBox", true)
   ButtonSetText("TalismanGenieSearchButton", L"Search")
   ButtonSetText("TalismanGenieResetButton", L"Reset")   
   ButtonSetText("TalismanGeniePrevButton", L"<< Prev")
   ButtonSetText("TalismanGenieNextButton", L"Next >>")   
   
   FragmentSkillLevel = {
		   { value = "1" }
		 , { value = "25" }
		 , { value = "50" }
		 , { value = "75" }
		 , { value = "100" }
		 , { value = "125" }
		 , { value = "150" }
		 , { value = "175" }
		 , { value = "200" }
		}

	SkillLevel = {
		   { value = "0" }
		 , { value = "1" }
		 , { value = "25" }
		 , { value = "50" }
		 , { value = "75" }
		 , { value = "100" }
		 , { value = "125" }
		 , { value = "150" }
		 , { value = "175" }
		 , { value = "200" }
		}

   TalismanData = {
		   { level = "1",  power_min = "2", power_max = "8", stat_common = "+3", stat_uncommon = "+7", stat_rare = "+11", stat_veryrare = "+15", armor_common="+22", armor_uncommon="+30", armor_rare="+37", armor_veryrare="+44", power_common="+5", power_uncommon="+11", power_rare="+17", power_veryrare="+23", resist_common="+10", resist_uncommon="+13", resist_rare="+16", resist_veryrare="+19" }
		 , { level = "4",  power_min = "9", power_max = "13", stat_common = "+4", stat_uncommon = "+8", stat_rare = "+12", stat_veryrare = "+16", armor_common="+30", armor_uncommon="+40", armor_rare="+50", armor_veryrare="+60", power_common="+6", power_uncommon="+12", power_rare="+18", power_veryrare="+24", resist_common="+15", resist_uncommon="+20", resist_rare="+24", resist_veryrare="+28" }
		 , { level = "8",  power_min = "14", power_max = "18", stat_common = "+5", stat_uncommon = "+9", stat_rare = "+13", stat_veryrare = "+17", armor_common="+37", armor_uncommon="+50", armor_rare="+62", armor_veryrare="+74", power_common="+8", power_uncommon="+14", power_rare="+20", power_veryrare="+26", resist_common="+20", resist_uncommon="+26", resist_rare="+31", resist_veryrare="+37" }
		 , { level = "12", power_min = "19", power_max = "23", stat_common = "+6", stat_uncommon = "+10", stat_rare = "+14", stat_veryrare = "+18", armor_common="+45", armor_uncommon="+60", armor_rare="+75", armor_veryrare="+90", power_common="+9", power_uncommon="+15", power_rare="+21", power_veryrare="+27", resist_common="+25", resist_uncommon="+32", resist_rare="+39", resist_veryrare="+46" }
		 , { level = "16", power_min = "24", power_max = "28", stat_common = "+7", stat_uncommon = "+11", stat_rare = "+15", stat_veryrare = "+19", armor_common="+52", armor_uncommon="+70", armor_rare="+87", armor_veryrare="+104", power_common="+11", power_uncommon="+17", power_rare="+23", power_veryrare="+29", resist_common="+30", resist_uncommon="+39", resist_rare="+47", resist_veryrare="+55" }
		 , { level = "20", power_min = "29", power_max = "33", stat_common = "+8", stat_uncommon = "+12", stat_rare = "+16", stat_veryrare = "+20", armor_common="+60", armor_uncommon="+80", armor_rare="+100", armor_veryrare="+120", power_common="+12", power_uncommon="+18", power_rare="+24", power_veryrare="+30", resist_common="+35", resist_uncommon="+45", resist_rare="+55", resist_veryrare="+64" }
		 , { level = "24", power_min = "34", power_max = "38", stat_common = "+9", stat_uncommon = "+13", stat_rare = "+17", stat_veryrare = "+21", armor_common="+67", armor_uncommon="+90", armor_rare="+112", armor_veryrare="+134", power_common="+14", power_uncommon="+20", power_rare="+26", power_veryrare="+32", resist_common="+40", resist_uncommon="+51", resist_rare="+62", resist_veryrare="+73" }
		 , { level = "28", power_min = "39", power_max = "44", stat_common = "+10", stat_uncommon = "+14", stat_rare = "+18", stat_veryrare = "+22", armor_common="+75", armor_uncommon="+100", armor_rare="+125", armor_veryrare="+150", power_common="+15", power_uncommon="+21", power_rare="+27", power_veryrare="+33", resist_common="+45", resist_uncommon="+58", resist_rare="+70", resist_veryrare="+82" }
		 , { level = "32", power_min = "45", power_max = "46", stat_common = "+11", stat_uncommon = "+15", stat_rare = "+19", stat_veryrare = "+23", armor_common="+82", armor_uncommon="+110", armor_rare="+137", armor_veryrare="+164", power_common="+17", power_uncommon="+23", power_rare="+29", power_veryrare="+35", resist_common="+50", resist_uncommon="+64", resist_rare="+78", resist_veryrare="+91" }
		 , { level = "36", power_min = "46", power_max = "47", stat_common = "+12", stat_uncommon = "+16", stat_rare = "+20", stat_veryrare = "+24", armor_common="+90", armor_uncommon="+120", armor_rare="+150", armor_veryrare="+180", power_common="+18", power_uncommon="+24", power_rare="+30", power_veryrare="+36", resist_common="+55", resist_uncommon="+70", resist_rare="+85", resist_veryrare="+100" }
   }
	
   TalismanTypes = {
		   { kind = "Stat" }
		 , { kind = "Armor" }
		 , { kind = "Power" }
		 , { kind = "Resist" }
		 , { kind = "Critical" }
   }
	CreateWindow("TalismanGenieIcon", true)
	WindowSetShowing("TalismanGenieIcon", true)
    LayoutEditor.RegisterWindow ( "TalismanGenieIcon"             -- Window Name
                                , L"TalismanGenieIcon"            -- Window Display Name
                                , L"Little Icon"              -- Window Description
                                , false                       -- Allow Size Width
                                , false                       -- Allow Size Height
                                , true                        -- Allow Hiding
                                , nil                         -- Allowable Anchor List
                                )

    LayoutEditor.RegisterWindow ( "TalismanGenie"             -- Window Name
                                , L"TalismanGenie"            -- Window Display Name
                                , L"TalismanGenie"            -- Window Description
                                , false                       -- Allow Size Width
                                , false                       -- Allow Size Height
                                , true                        -- Allow Hiding
                                , nil                         -- Allowable Anchor List
                                )
    TalismanGenie.Reset()
end

function TalismanGenie.Reset()
    ComboBoxClearMenuItems("TalismanGenieLevelComboBox")    
    ComboBoxClearMenuItems("TalismanGenieTypeComboBox")
    ComboBoxClearMenuItems("TalismanGenieFragmentMinComboBox")
    ComboBoxClearMenuItems("TalismanGenieFragmentMaxComboBox")
    ComboBoxClearMenuItems("TalismanGenieGoldDustMinComboBox")
    ComboBoxClearMenuItems("TalismanGenieGoldDustMaxComboBox")
    ComboBoxClearMenuItems("TalismanGenieEssenceMinComboBox")
    ComboBoxClearMenuItems("TalismanGenieEssenceMaxComboBox")
    ComboBoxClearMenuItems("TalismanGenieCurioMinComboBox")
    ComboBoxClearMenuItems("TalismanGenieCurioMaxComboBox")
    ButtonSetPressedFlag("TalismanGenieContainerCheckBox", false)
    ButtonSetPressedFlag("TalismanGenieGoldDustCheckBox", false)
    ButtonSetPressedFlag("TalismanGenieEssenceCheckBox", false)
    ButtonSetPressedFlag("TalismanGenieCurioCheckBox", false)

    LabelSetText("TalismanGenieTotalFoundLabel", L"Total Found:")
    LabelSetText("TalismanGenieDisplayingLabel", L"Displaying:")


    for recipewipe=1,25
    do
        LabelSetText("TalismanGenieContainerRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieFragmentRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieGoldDustRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieEssenceRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieCurioRecipeLabel"..recipewipe, L"")
    end
    
    for i, v in ipairs(TalismanData) do
	    ComboBoxAddMenuItem("TalismanGenieLevelComboBox", towstring(v.level))
	end

    for i, v in ipairs(TalismanTypes) do
	    ComboBoxAddMenuItem("TalismanGenieTypeComboBox", towstring(v.kind))
	end	

	
    for i, v in ipairs(FragmentSkillLevel) do
	    ComboBoxAddMenuItem("TalismanGenieFragmentMinComboBox", towstring(v.value))
	    ComboBoxAddMenuItem("TalismanGenieFragmentMaxComboBox", towstring(v.value))
	end
	
	for i, v in ipairs(SkillLevel) do
        ComboBoxAddMenuItem("TalismanGenieGoldDustMinComboBox", towstring(v.value))
	    ComboBoxAddMenuItem("TalismanGenieGoldDustMaxComboBox", towstring(v.value))
	    ComboBoxAddMenuItem("TalismanGenieEssenceMinComboBox", towstring(v.value))
	    ComboBoxAddMenuItem("TalismanGenieEssenceMaxComboBox", towstring(v.value))
	    ComboBoxAddMenuItem("TalismanGenieCurioMinComboBox", towstring(v.value))
	    ComboBoxAddMenuItem("TalismanGenieCurioMaxComboBox", towstring(v.value))
	end
	
	ComboBoxSetSelectedMenuItem("TalismanGenieLevelComboBox", 1)
	ComboBoxSetSelectedMenuItem("TalismanGenieTypeComboBox", 1)
	ComboBoxSetSelectedMenuItem("TalismanGenieFragmentMinComboBox", 1)
	ComboBoxSetSelectedMenuItem("TalismanGenieFragmentMaxComboBox", 9)
	ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMinComboBox", 1)
	ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMaxComboBox", 10)
	ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMinComboBox", 1)
	ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMaxComboBox", 10)
	ComboBoxSetSelectedMenuItem("TalismanGenieCurioMinComboBox", 1)
	ComboBoxSetSelectedMenuItem("TalismanGenieCurioMaxComboBox", 10)

	TotalCount=0
	searchoffset=1
	isFragmentReversed=false
	isGoldDustReversed=false
	isEssenceReversed=false
	isCurioReversed=false	
	TalismanFound={}
	TalismanGenie.TalismanStats()

end

function TalismanGenie.TalismanStats()
    what_talisman_level=ComboBoxGetSelectedMenuItem("TalismanGenieLevelComboBox")
    which_tailisman_type=ComboBoxGetSelectedMenuItem("TalismanGenieTypeComboBox")

    --Stat Talisman    
    if (which_tailisman_type == 1) then
        LabelSetText("TalismanGenieCommonLabel", L""..towstring(TalismanData[what_talisman_level].stat_common))
        LabelSetText("TalismanGenieUncommonLabel", L""..towstring(TalismanData[what_talisman_level].stat_uncommon))
        LabelSetText("TalismanGenieRareLabel", L""..towstring(TalismanData[what_talisman_level].stat_rare))
        LabelSetText("TalismanGenieVeryRareLabel", L""..towstring(TalismanData[what_talisman_level].stat_veryrare))
    end

    --Armor Talisman    
    if (which_tailisman_type == 2) then
        LabelSetText("TalismanGenieCommonLabel", L""..towstring(TalismanData[what_talisman_level].armor_common))
        LabelSetText("TalismanGenieUncommonLabel", L""..towstring(TalismanData[what_talisman_level].armor_uncommon))
        LabelSetText("TalismanGenieRareLabel", L""..towstring(TalismanData[what_talisman_level].armor_rare))
        LabelSetText("TalismanGenieVeryRareLabel", L""..towstring(TalismanData[what_talisman_level].armor_veryrare))
    end

    --Power Talisman    
    if (which_tailisman_type == 3) then
        LabelSetText("TalismanGenieCommonLabel", L""..towstring(TalismanData[what_talisman_level].power_common))
        LabelSetText("TalismanGenieUncommonLabel", L""..towstring(TalismanData[what_talisman_level].power_uncommon))
        LabelSetText("TalismanGenieRareLabel", L""..towstring(TalismanData[what_talisman_level].power_rare))
        LabelSetText("TalismanGenieVeryRareLabel", L""..towstring(TalismanData[what_talisman_level].power_veryrare))
    end

    --Resist Talisman    
    if (which_tailisman_type == 4) then
        LabelSetText("TalismanGenieCommonLabel", L""..towstring(TalismanData[what_talisman_level].resist_common))
        LabelSetText("TalismanGenieUncommonLabel", L""..towstring(TalismanData[what_talisman_level].resist_uncommon))
        LabelSetText("TalismanGenieRareLabel", L""..towstring(TalismanData[what_talisman_level].resist_rare))
        LabelSetText("TalismanGenieVeryRareLabel", L""..towstring(TalismanData[what_talisman_level].resist_veryrare))
    end
    
    --Critical Talisman    
    if (which_tailisman_type == 5) then
        LabelSetText("TalismanGenieCommonLabel", L"1%")
        LabelSetText("TalismanGenieUncommonLabel", L"2%")
        LabelSetText("TalismanGenieRareLabel", L"3%")
        LabelSetText("TalismanGenieVeryRareLabel", L"4%")
    end
end

function TalismanGenie.Search()
    what_talisman_level=ComboBoxGetSelectedMenuItem("TalismanGenieLevelComboBox")
    lowpower=tonumber(TalismanData[what_talisman_level].power_min)
    highpower=tonumber(TalismanData[what_talisman_level].power_max)
    
    searchoffset=1
    containerskill=0
    fragmentskill=0
    golddustskill=0
    curioskill=0
    essenceskill=0
    TotalCount=0
    fragment_min=ComboBoxGetSelectedMenuItem("TalismanGenieFragmentMinComboBox")
    fragment_max=ComboBoxGetSelectedMenuItem("TalismanGenieFragmentMaxComboBox")
    golddust_min=ComboBoxGetSelectedMenuItem("TalismanGenieGoldDustMinComboBox")-1
    golddust_max=ComboBoxGetSelectedMenuItem("TalismanGenieGoldDustMaxComboBox")-1
    essence_min=ComboBoxGetSelectedMenuItem("TalismanGenieEssenceMinComboBox")-1
    essence_max=ComboBoxGetSelectedMenuItem("TalismanGenieEssenceMaxComboBox")-1
    curio_min=ComboBoxGetSelectedMenuItem("TalismanGenieCurioMinComboBox")-1
    curio_max=ComboBoxGetSelectedMenuItem("TalismanGenieCurioMaxComboBox")-1
    LabelSetText("TalismanGenieNoteLabel", L"")   
    
    if (ButtonGetPressedFlag("TalismanGenieContainerCheckBox")==true) then
       use200container=1
    else
        use200container=0
    end    
    TalismanFound={}
    
    for containerbase=0,use200container
    do
        containerskill=containerbase * 200
        if (containerskill == 0) then containerskill = "1-175" end
        containerpower=containerbase

        for fragmentbase=fragment_min,fragment_max
        do
            fragmentskill=(fragmentbase - 1) * 25
            if (fragmentskill == 0) then fragmentskill = 1 end
            golddustskill=0
            curioskill=0
            essenceskill=0
            fragmentpower = fragmentbase * 2

            for curiobase=curio_min,curio_max
            do
                curiopower=curiobase
                curioskill=(curiobase - 1) * 25
                if (curioskill == -25) then curioskill = 0
                elseif (curioskill == 0) then curioskill = 1 end        
        
                for essencebase=essence_min,essence_max
                do
                    essencepower=essencebase
                    essenceskill=(essencebase - 1) * 25
                    if (essenceskill == -25) then essenceskill = 0 
                    elseif (essenceskill == 0) then essenceskill = 1 end                        

                    for golddustbase=golddust_min,golddust_max
                    do                
                        golddustpower=golddustbase
                        golddustskill=(golddustbase - 1) * 25
                        if (golddustskill == -25) then golddustskill = 0
                        elseif (golddustskill == 0) then golddustskill = 1 end
                    
                        totalpower=containerpower + fragmentpower + golddustpower +  essencepower + curiopower
                        if (totalpower >= lowpower) and (totalpower <= highpower) then 
                            newentry={
                                container=containerskill,
                                fragment=fragmentskill,
                                golddust=golddustskill,
                                essence=essenceskill,
                                curio=curioskill
                            }
                            table.insert(TalismanFound, newentry)
                            TotalCount = TotalCount + 1                        
                        end
                    end
                end
            end
        end
    end    
        
    TalismanGenie.DisplayData()
end

function TalismanGenie.FragmentChange()
        current_min_value=ComboBoxGetSelectedMenuItem("TalismanGenieFragmentMinComboBox")
        current_max_value=ComboBoxGetSelectedMenuItem("TalismanGenieFragmentMaxComboBox")
        
        if (current_min_value > current_max_value) then 
            ComboBoxSetSelectedMenuItem("TalismanGenieFragmentMaxComboBox", current_min_value)
        end
end

function TalismanGenie.GoldDustChange()
        current_min_value=ComboBoxGetSelectedMenuItem("TalismanGenieGoldDustMinComboBox")
        current_max_value=ComboBoxGetSelectedMenuItem("TalismanGenieGoldDustMaxComboBox")
        
        if (current_max_value == 1) then 
            ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMinComboBox", 1)
            current_min_value=1
        end

        if (current_min_value > current_max_value) then 
            ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMaxComboBox", current_min_value)
        end
end

function TalismanGenie.EssenceChange()
        current_min_value=ComboBoxGetSelectedMenuItem("TalismanGenieEssenceMinComboBox")
        current_max_value=ComboBoxGetSelectedMenuItem("TalismanGenieEssenceMaxComboBox")

        if (current_max_value == 1) then 
            ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMinComboBox", 1)
            current_min_value=1
        end                
        
        if (current_min_value > current_max_value) then 
            ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMaxComboBox", current_min_value)
        end
end

function TalismanGenie.CurioChange()
        current_min_value=ComboBoxGetSelectedMenuItem("TalismanGenieCurioMinComboBox")
        current_max_value=ComboBoxGetSelectedMenuItem("TalismanGenieCurioMaxComboBox")
        
        if (current_max_value == 1) then 
            ComboBoxSetSelectedMenuItem("TalismanGenieCurioMinComboBox", 1)
            current_min_value=1
        end                

        if (current_min_value > current_max_value) then 
            ComboBoxSetSelectedMenuItem("TalismanGenieCurioMaxComboBox", current_min_value)
        end
end

function TalismanGenie.GoldDustCheckToggle()
CheckStatus = ButtonGetPressedFlag("TalismanGenieGoldDustCheckBox")    
    if (CheckStatus == true) then  
        ComboBoxClearMenuItems("TalismanGenieGoldDustMinComboBox")
        ComboBoxClearMenuItems("TalismanGenieGoldDustMaxComboBox")
        ComboBoxAddMenuItem("TalismanGenieGoldDustMinComboBox", L"0")
        ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMinComboBox", 1)
        ComboBoxAddMenuItem("TalismanGenieGoldDustMaxComboBox", L"0")
        ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMaxComboBox", 1)
    else
        ComboBoxClearMenuItems("TalismanGenieGoldDustMinComboBox")
        ComboBoxClearMenuItems("TalismanGenieGoldDustMaxComboBox")
        for i, v in ipairs(SkillLevel) do
            ComboBoxAddMenuItem("TalismanGenieGoldDustMinComboBox", towstring(v.value))
            ComboBoxAddMenuItem("TalismanGenieGoldDustMaxComboBox", towstring(v.value))
	    end
	    ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMinComboBox", 1)
	    ComboBoxSetSelectedMenuItem("TalismanGenieGoldDustMaxComboBox", 10)
    end    
end

function TalismanGenie.EssenceCheckToggle()
CheckStatus = ButtonGetPressedFlag("TalismanGenieEssenceCheckBox")    
    if (CheckStatus == true) then  
        ComboBoxClearMenuItems("TalismanGenieEssenceMinComboBox")
        ComboBoxClearMenuItems("TalismanGenieEssenceMaxComboBox")
        ComboBoxAddMenuItem("TalismanGenieEssenceMinComboBox", L"0")
        ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMinComboBox", 1)
        ComboBoxAddMenuItem("TalismanGenieEssenceMaxComboBox", L"0")
        ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMaxComboBox", 1)
    else
        ComboBoxClearMenuItems("TalismanGenieEssenceMinComboBox")
        ComboBoxClearMenuItems("TalismanGenieEssenceMaxComboBox")
        for i, v in ipairs(SkillLevel) do
            ComboBoxAddMenuItem("TalismanGenieEssenceMinComboBox", towstring(v.value))
            ComboBoxAddMenuItem("TalismanGenieEssenceMaxComboBox", towstring(v.value))
	    end
	    ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMinComboBox", 1)
	    ComboBoxSetSelectedMenuItem("TalismanGenieEssenceMaxComboBox", 10)
    end    
end

function TalismanGenie.CurioCheckToggle()
CheckStatus = ButtonGetPressedFlag("TalismanGenieCurioCheckBox")    
    if (CheckStatus == true) then  
        ComboBoxClearMenuItems("TalismanGenieCurioMinComboBox")
        ComboBoxClearMenuItems("TalismanGenieCurioMaxComboBox")
        ComboBoxAddMenuItem("TalismanGenieCurioMinComboBox", L"0")
        ComboBoxSetSelectedMenuItem("TalismanGenieCurioMinComboBox", 1)
        ComboBoxAddMenuItem("TalismanGenieCurioMaxComboBox", L"0")
        ComboBoxSetSelectedMenuItem("TalismanGenieCurioMaxComboBox", 1)
    else
        ComboBoxClearMenuItems("TalismanGenieCurioMinComboBox")
        ComboBoxClearMenuItems("TalismanGenieCurioMaxComboBox")
        for i, v in ipairs(SkillLevel) do
            ComboBoxAddMenuItem("TalismanGenieCurioMinComboBox", towstring(v.value))
            ComboBoxAddMenuItem("TalismanGenieCurioMaxComboBox", towstring(v.value))
	    end
	    ComboBoxSetSelectedMenuItem("TalismanGenieCurioMinComboBox", 1)
	    ComboBoxSetSelectedMenuItem("TalismanGenieCurioMaxComboBox", 10)
    end    
end

function TalismanGenie.GoPrev()
    if ((searchoffset-25)<=1) then
        searchoffset=1
    else
        searchoffset=searchoffset-25
    end
    TalismanGenie.DisplayData()
end

function TalismanGenie.GoNext()
    if ((searchoffset+25)>TotalCount) then
        searchoffset=searchoffset
    else    
        searchoffset=searchoffset+25        
    end
    TalismanGenie.DisplayData()
end

function TalismanGenie.DisplayData()
    for recipewipe=1,25
    do
        LabelSetText("TalismanGenieContainerRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieFragmentRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieGoldDustRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieEssenceRecipeLabel"..recipewipe, L"")
        LabelSetText("TalismanGenieCurioRecipeLabel"..recipewipe, L"")
    end        

    lowend_search=searchoffset
    if ((lowend_search+25)>TotalCount) then        
        lowend_search=searchoffset
        highend_search=TotalCount
    else
        highend_search=searchoffset+24
    end
    
    if (TotalCount==0) then 
        display_text="Displaying:"
        TotalCount=""
    else
        display_text=string.format("Displaying: %s-%s", lowend_search, highend_search)
    end    
    LabelSetText("TalismanGenieTotalFoundLabel", L"Total Found: "..towstring(TotalCount))
    LabelSetText("TalismanGenieDisplayingLabel", L""..towstring(display_text))
    for current_value=1,25
    do
        search_value=current_value+searchoffset-1

        if (search_value > TotalCount) then
            LabelSetText("TalismanGenieContainerRecipeLabel"..current_value, L"")
            LabelSetText("TalismanGenieFragmentRecipeLabel"..current_value, L"")
            LabelSetText("TalismanGenieGoldDustRecipeLabel"..current_value, L"")
            LabelSetText("TalismanGenieEssenceRecipeLabel"..current_value, L"")
            LabelSetText("TalismanGenieCurioRecipeLabel"..current_value, L"")
        else
            LabelSetText("TalismanGenieContainerRecipeLabel"..current_value, L""..towstring(TalismanFound[search_value].container))
            LabelSetText("TalismanGenieFragmentRecipeLabel"..current_value, L""..towstring(TalismanFound[search_value].fragment))
            LabelSetText("TalismanGenieGoldDustRecipeLabel"..current_value, L""..towstring(TalismanFound[search_value].golddust))
            LabelSetText("TalismanGenieEssenceRecipeLabel"..current_value, L""..towstring(TalismanFound[search_value].essence))
            LabelSetText("TalismanGenieCurioRecipeLabel"..current_value, L""..towstring(TalismanFound[search_value].curio))            
        end
        
        if ((current_value % 2)==1) then
            LabelSetTextColor("TalismanGenieContainerRecipeLabel"..current_value, 192, 102, 0)
            LabelSetTextColor("TalismanGenieFragmentRecipeLabel"..current_value, 192, 102, 0)
            LabelSetTextColor("TalismanGenieGoldDustRecipeLabel"..current_value, 192, 102, 0)
            LabelSetTextColor("TalismanGenieEssenceRecipeLabel"..current_value, 192, 102, 0)
            LabelSetTextColor("TalismanGenieCurioRecipeLabel"..current_value, 192, 102, 0)
        end
        
    end
    
    local checklevel=ComboBoxGetSelectedMenuItem("TalismanGenieLevelComboBox")
    if (use200container==1 and checklevel==10) then 
        LabelSetText("TalismanGenieNoteLabel", L"NOTE: You need to get a critical to make them")    
    end
end

function TalismanGenie.Toggle()
	if not isVisible then
		WindowSetShowing("TalismanGenie", true)
		isVisible=true
	else
		WindowSetShowing("TalismanGenie", false)
		isVisible=false
	end
end

function TalismanGenie.LevelMouseOver()
        msg="Please choose the rank of Talisman you wish to make."
        Tooltips.CreateTextOnlyTooltip( "TalismanGenieLevelLabel", nil )
        Tooltips.SetTooltipText( 1, 1, towstring(msg))
        Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
        Tooltips.Finalize()
        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )
end

function TalismanGenie.FragmentMouseOver()
        msg="Fragments are the most important piece. The color of the Fragment determines what color it becomes.\n\nMost criticals will make the talisman a rank higher.\n\nA super-critial does that and changes the color to one rank higher.\n\nIn the case of a Purple, the fragment becomes unused, since there is nothing higher than it."
        Tooltips.CreateTextOnlyTooltip( "TalismanGenieFragmentLabel", nil )
        Tooltips.SetTooltipText( 1, 1, towstring(msg))
        Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
        Tooltips.Finalize()
        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )
end

function TalismanGenie.ContainerMouseOver()
        msg="Containers only help to make a critical happen, however you need 200s to make Level 36 Talismen.\n\nSkill 1 Container: 1% Critical bonus.\nSkill 50 Container: 2% Critical bonus.\nSkill 100 Container: 3% Critical bonus.\nSkill 150 Container: 4% Critical bonus.\nSkill 200 Container: 5% Critical bonus.\n"
        Tooltips.CreateTextOnlyTooltip( "TalismanGenieContainerLabel", nil )
        Tooltips.SetTooltipText( 1, 1, towstring(msg))
        Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
        Tooltips.Finalize()
        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )
end

function TalismanGenie.TypeMouseOver()
        msg="The type of Talisman you wish to make.\n\nStat: Your typical Talisman.\nArmor: Physical damage reduction.\nPower: Melee, Ranged, Healing or Magic.\nResist: Magical resistance Talismans.\nCritical: Critical Talismen type."
        Tooltips.CreateTextOnlyTooltip( "TalismanGenieTypeLabel", nil )
        Tooltips.SetTooltipText( 1, 1, towstring(msg))
        Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
        Tooltips.Finalize()
        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )
end

function TalismanGenie.FragmentToggle()
    LabelSetTextColor("TalismanGenieFragmentColumnLabel", 146, 56, 208)
	if not isFragmentReversed then
		isFragmentReversed=true
		table.sort(TalismanFound, function (a,b) return a.fragment>b.fragment end)		
	else
		isFragmentReversed=false
		table.sort(TalismanFound, function (a,b) return a.fragment<b.fragment end)
	end
    searchoffset=1
    TalismanGenie.DisplayData()	
end

function TalismanGenie.GoldDustToggle()
    LabelSetTextColor("TalismanGenieGoldDustColumnLabel", 146, 56, 208)
	if not isGoldDustReversed then
		isGoldDustReversed=true
		table.sort(TalismanFound, function (a,b) return a.golddust>b.golddust end)
	else
		isGoldDustReversed=false
		table.sort(TalismanFound, function (a,b) return a.golddust<b.golddust end)		
	end
    searchoffset=1
    TalismanGenie.DisplayData()	
end

function TalismanGenie.EssenceToggle()
    LabelSetTextColor("TalismanGenieEssenceColumnLabel", 146, 56, 208)
	if not isEssenceReversed then
		isEssenceReversed=true
		table.sort(TalismanFound, function (a,b) return a.essence>b.essence end)		
	else
		isEssenceReversed=false
		table.sort(TalismanFound, function (a,b) return a.essence<b.essence end)
	end
    searchoffset=1
    TalismanGenie.DisplayData()
end

function TalismanGenie.CurioToggle()
    LabelSetTextColor("TalismanGenieCurioColumnLabel", 146, 56, 208)
	if not isCurioReversed then
	    isCurioReversed=true
	    table.sort(TalismanFound, function (a,b) return a.curio>b.curio end)
	else
		isCurioReversed=false
		table.sort(TalismanFound, function (a,b) return a.curio<b.curio end)		
	end
    searchoffset=1
    TalismanGenie.DisplayData()	
end

function TalismanGenie.FragmentOn()
    LabelSetTextColor("TalismanGenieFragmentColumnLabel", 123, 172, 220)
    msg="Click to sort results by Fragment."
    Tooltips.CreateTextOnlyTooltip( "TalismanGenieFragmentColumnLabel", nil )
    Tooltips.SetTooltipText( 1, 1, towstring(msg))
    Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
    Tooltips.Finalize()
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )    
end

function TalismanGenie.FragmentOff()
    LabelSetTextColor("TalismanGenieFragmentColumnLabel", 255, 204, 102)
end

function TalismanGenie.GoldDustOn()
    LabelSetTextColor("TalismanGenieGoldDustColumnLabel", 123, 172, 220)
    msg="Click to sort results by Gold Dust."
    Tooltips.CreateTextOnlyTooltip( "TalismanGenieGoldDustColumnLabel", nil )
    Tooltips.SetTooltipText( 1, 1, towstring(msg))
    Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
    Tooltips.Finalize()
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )    
end

function TalismanGenie.GoldDustOff()
    LabelSetTextColor("TalismanGenieGoldDustColumnLabel", 255, 204, 102)
end

function TalismanGenie.EssenceOn()
    LabelSetTextColor("TalismanGenieEssenceColumnLabel", 123, 172, 220)
    msg="Click to sort results by Essence."
    Tooltips.CreateTextOnlyTooltip( "TalismanGenieEssenceColumnLabel", nil )
    Tooltips.SetTooltipText( 1, 1, towstring(msg))
    Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
    Tooltips.Finalize()
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )    
end

function TalismanGenie.EssenceOff()
    LabelSetTextColor("TalismanGenieEssenceColumnLabel", 255, 204, 102)
end

function TalismanGenie.CurioOn()
    LabelSetTextColor("TalismanGenieCurioColumnLabel", 123, 172, 220)
    msg="Click to sort results by Curio."
    Tooltips.CreateTextOnlyTooltip( "TalismanGenieCurioColumnLabel", nil )
    Tooltips.SetTooltipText( 1, 1, towstring(msg))
    Tooltips.SetTooltipColor( 1, 1, 123, 172, 220 )
    Tooltips.Finalize()
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )    
end

function TalismanGenie.CurioOff()
    LabelSetTextColor("TalismanGenieCurioColumnLabel", 255, 204, 102)
end