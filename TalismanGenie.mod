<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="TalismanGenie" version="1.0" date="01/10/2009" >
        <Author name="RealLifeGobbo" email="reallifegobbo@gmail.com" />
        <Description text="Tried of trying to figure out what combination of components you need to make talismans? Now you don't have to with TalismanGenie!" />
        <VersionSettings gameVersion="1.4.0" windowsVersion="1.0"/>
        <Dependencies>
	    <Dependency name="EASystem_Utils"/>
            <Dependency name="EASystem_WindowUtils" />
        </Dependencies>
        <Files>
            <File name="TalismanGenie.lua" />
	    <File name="TalismanGenie.xml" />
        </Files>     
        <OnInitialize>
            <CallFunction name="TalismanGenie.Initialise" />
        </OnInitialize>
        <OnUpdate/>
        <OnShutdown/>
        <WARInfo>
    <Categories>
        <Category name="CRAFTING" />
        <Category name="OTHER" />
    </Categories>
    <Careers>
        <Career name="BLACKGUARD" />
        <Career name="WITCH_ELF" />
        <Career name="DISCIPLE" />
        <Career name="SORCERER" />
        <Career name="IRON_BREAKER" />
        <Career name="SLAYER" />
        <Career name="RUNE_PRIEST" />
        <Career name="ENGINEER" />
        <Career name="BLACK_ORC" />
        <Career name="CHOPPA" />
        <Career name="SHAMAN" />
        <Career name="SQUIG_HERDER" />
        <Career name="WITCH_HUNTER" />
        <Career name="KNIGHT" />
        <Career name="BRIGHT_WIZARD" />
        <Career name="WARRIOR_PRIEST" />
        <Career name="CHOSEN" />
        <Career name= "MARAUDER" />
        <Career name="ZEALOT" />
        <Career name="MAGUS" />
        <Career name="SWORDMASTER" />
        <Career name="SHADOW_WARRIOR" />
        <Career name="WHITE_LION" />
        <Career name="ARCHMAGE" />
    </Careers>
</WARInfo>

    </UiMod>
</ModuleFile>
